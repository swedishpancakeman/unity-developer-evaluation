﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This class provides a way for checking to see if there are restrictions on which parts can be added alongside other parts.
/// A list of incompatible part pairs can be assigned through the inspector of this component.
/// </summary>
public class PartCompatibilityValidator : MonoBehaviour
{
    #region inspector variables
    public List<IncompatibilityPartPair> IncompatiblePartList;
    #endregion

    private PartManager partManager;
    
    /// <summary>
    /// Initializes this class with part manager reference
    /// </summary>
    /// <param name="partManager">The part manager</param>
    public void Init(PartManager partManager)
    {
        this.partManager = partManager;
    }

    /// <summary>
    /// Pair of two parts that are not compatible with one another, may have also been able to use a tuple here...
    /// </summary>
    [Serializable]
    public class IncompatibilityPartPair
    {
        // unity's inspector uses the name field to replace "Element 0" with the value assigned to this field in the inspector
        [HideInInspector]
        public string name;

        public Part part1;
        public Part part2;
    }

    /// <summary>
    /// Checks to see if two parts are compatible with one another
    /// </summary>
    /// <param name="newPart">The new part that we are trying to replace the existing part with</param>
    /// <param name="incompatiblePart">Populated with the opposing incompatible part if found to be incompatible with newPart</param>
    /// <returns>True if the part is compatible with the existing part</returns>
    public bool HasCompatibility(Part newPart, out Part incompatiblePart)
    {
        // check to see if this part exists in the incompatibility list at all (as either part 1 or part 2), if it doesn't exist then there's no rules against adding it
        var exists = IncompatiblePartList.Any(pair => pair.part1.name == newPart.name || pair.part2.name == newPart.name);
        incompatiblePart = null;

        if (!exists)
        {
            // we don't need to check any further, it's compatible
            return true;
        }
        
        // otherwise... we know that it exists in the list somewhere, now check the part manager to see if its other half has already been added

        // check the first side, does the part match any parts in the first field?
        var matchesPart1 = IncompatiblePartList.Where(pair => pair.part1.name == newPart.name);
        foreach(var match in matchesPart1)
        {
            if(partManager.ContainsCurrentPart(match.part2))
            {
                incompatiblePart = match.part2;
                return false;
            }
        }

        // check the second side, does the part match any parts in the second field?
        var matchesPart2 = IncompatiblePartList.Where(pair => pair.part2.name == newPart.name);
        foreach (var match in matchesPart2)
        {
            if (partManager.ContainsCurrentPart(match.part1))
            {
                incompatiblePart = match.part1;
                return false;
            }
        }

        return true;
    }
}