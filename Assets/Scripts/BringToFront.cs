﻿using UnityEngine;

/// <summary>
/// Places the modal on top of all other UI elements when it is enabled
/// </summary>
public class BringToFront : MonoBehaviour
{
    /// <summary>
    /// Fires when the gameobject is enabled
    /// </summary>
    void OnEnable()
    {
        transform.SetAsLastSibling();
    }
}
