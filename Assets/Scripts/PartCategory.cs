﻿using System.ComponentModel;

/// <summary>
/// Represents all possible part categories
/// </summary>
public enum PartCategory
{
    Body = 0,
    [Description("Front Armor")]
    FrontArmor = 1,
    [Description("Back Armor")]
    BackArmor = 2,
    [Description("Front Bumper")]
    FrontBumper = 3,
    [Description("Back Bumper")]
    BackBumper = 4,
    [Description("Front Lights")]
    FrontLights = 5,
    [Description("Back Lights")]
    BackLights = 6,
    [Description("Top Armor")]
    TopArmor = 7,
    Wheels = 8,
    Suspension = 9,
    Spoiler = 10,
    Motor = 11,
    Decoration = 12,
    Brakes = 13
}
