﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Responsible for keeping track of which parts are currently in the configuration, getting the parts by category,
/// and saving/loading/listing configurations.
/// </summary>
public class PartManager : MonoBehaviour
{
    #region inspector variables
    public List<Part> partPrefabs;
    public List<Part> currentConfigurationParts;
    #endregion

    private DbContext dbContext;

    void Awake()
    {
        dbContext = new DbContext();
    }

    /// <summary>
    /// Takes inventory of the parts that exist under the customVehicleRoot when the app first starts up
    /// </summary>
    /// <param name="customVehicleRoot">The root transform of the current configuration</param>
    public void InitializeCurrentPartsList(GameObject customVehicleRoot)
    {
        // take the sequence produced by Cast<Transform>(), iterate over each element, project into an IEnumerable and then flatten once again giving
        // us the a list of Part components for each of the direct children of the custom vehicle root
        var directChildrenPartComponents = customVehicleRoot.transform.Cast<Transform>().SelectMany(transform => transform.GetComponents<Part>()).ToList();
        
        currentConfigurationParts.AddRange(directChildrenPartComponents);
    }

    /// <summary>
    /// Gets all of the parts for a specific category except for the part that is currently selected
    /// </summary>
    /// <param name="category">The category of the part</param>
    /// <param name="currentPart">The current part that is selected</param>
    /// <returns>A list of parts other than the currently selected one</returns>
    public List<Part> GetPartsByCategory(PartCategory category, Part currentPart)
    {
        return partPrefabs.Where(part =>
                part.category == category &&
                !String.Equals(part.name, currentPart.gameObject.name, StringComparison.CurrentCultureIgnoreCase)
            )
            .Select(part => part.GetComponent<Part>())
            .ToList();
    }

    public List<Part> GetAllPartsByCategory(PartCategory category)
    {
        return partPrefabs.Where(part => part.category == category)
                        .Select(part => part.GetComponent<Part>())
                        .ToList();
    }

    /// <summary>
    /// Gets a part by name
    /// </summary>
    /// <param name="name">The name of the part we wish to find</param>
    /// <returns>A part given a specific name</returns>
    public Part FindPartByName(string name)
    {
        return partPrefabs.Where(part => part.name == name)
            .Cast<Part>()
            .SingleOrDefault();
    }

    /// <summary>
    /// Saves a new configuration
    /// </summary>
    /// <param name="name">The name of the new configuration</param>
    public void SaveConfiguration(string name)
    {
        var json = HelperExtensions.JsonHelper.ToJson(currentConfigurationParts.Select(part => part.name).ToArray());
        Debug.Log("Saving: " + name + " with data: " + json);
        dbContext.SaveConfiguration(name, json);
    }

    /// <summary>
    /// Loads an existing configuration, given an ID
    /// </summary>
    /// <param name="id">The ID of the existing configuration in the database</param>
    /// <returns>A list of part names that can be used to instantiate prefabs from</returns>
    public List<string> LoadConfiguration(int id)
    {
        string data = dbContext.LoadConfiguration(id);
     
        // convert from json to a list of part (prefab) names
        List<string> partNames = HelperExtensions.JsonHelper.FromJson<string>(data).ToList();

        return partNames;
    }

    /// <summary>
    /// Returns a list of all saved configurations
    /// </summary>
    /// <returns></returns>
    public List<ConfigurationViewModel> ListSavedConfigurations()
    {
        return dbContext.GetSavedConfigurations();
    }

    #region current parts list bookkeeping
    /// <summary>
    /// Adds a new part to the list
    /// </summary>
    /// <param name="newPart">The new part</param>
    public void AddCurrentPart(Part newPart)
    {
        currentConfigurationParts.Add(newPart);
    }

    /// <summary>
    /// Removes a part from the list
    /// </summary>
    /// <param name="part">The part to remove</param>
    public void RemoveCurrentPart(Part part)
    {
        currentConfigurationParts.Remove(part);
    }

    /// <summary>
    /// Removes all parts from the list
    /// </summary>
    public void ClearAllParts()
    {
        currentConfigurationParts.Clear();
    }

    /// <summary>
    /// Returns a list of all parts for the existing configuration
    /// </summary>
    /// <returns></returns>
    public List<Part> GetCurrentParts()
    {
        return currentConfigurationParts;
    }

    /// <summary>
    /// Checks to see if the current configuration has a specific part
    /// </summary>
    /// <param name="part">The part to check to see if it exists</param>
    /// <returns></returns>
    public bool ContainsCurrentPart(Part part)
    {
        return currentConfigurationParts.Any(currentPart => currentPart.name == part.name);
    }
    #endregion
}
