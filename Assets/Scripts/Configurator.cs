﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

/// <summary>
/// The class that drives main logic for the app.
/// </summary>
[RequireComponent(typeof(PartManager))]
[RequireComponent(typeof(PartCompatibilityValidator))]
public class Configurator : MonoBehaviour
{
    #region inspector variables
    [Header("Scene Object References")]
    public Part currentPart = null;
    public GameObject customVehicleRoot;
    public PartManager partManager;
    public PartCompatibilityValidator partCompatibilityValidator;

    [Header("Materials")]
    public Material selectedMaterial;
    public Material incompatibleMaterial;

    [Header("UI Elements")]
    public ConfigurationPanel configurationPanel;

    [Header("Driveable Prefabs")]
    public GameObject bodyCollidersPrefab;
    public GameObject wheelsHubsPrefab;
    public GameObject carCamera;
    public GameObject environmentRoot;
    #endregion

    private Camera mainCamera;
    private GameObject drivingCameraRoot;
    private Transform driveableCenterMouseLookAtTarget;
    private bool isDriving = false;

    private const int LEFT_MOUSE_BUTTON = 0;
    private const float MAX_RAYCAST_DISTANCE = 10000.0f;

    /// <summary>
    /// Very simple singleton, we only really need one instance of Configurator to exist in this scene
    /// and it's easier to create this accessor and have other objects find it through Configurator.Instance than to
    /// go through individual game objects (and prefabs) and manually assign this component to them if they need to
    /// somehow reference it.
    /// </summary>
    private static Configurator _instance;
    public static Configurator Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Configurator>();
            if (_instance == null)
                Debug.LogError("Configurator instance cannot been not found.");
            return _instance;
        }
    }

    /// <summary>
    /// Sets static instance and validates various public inspector members to make sure they are properly assigned
    /// </summary>
    void Awake()
    {
        if (_instance == null)
            _instance = this as Configurator;

        ValidateInit();
    }

    /// <summary>
    /// Initialize other components that we will be working with
    /// </summary>
    private void Start()
    {
        partManager = GetComponent<PartManager>();
        partCompatibilityValidator = GetComponent<PartCompatibilityValidator>();

        partManager.InitializeCurrentPartsList(customVehicleRoot);

        partCompatibilityValidator.Init(partManager);

        configurationPanel.Init(ReplaceCurrentPart, partManager.SaveConfiguration, LoadConfiguration);

        mainCamera = Camera.main;
    }

    void Update ()
    {
        // if the left mouse button is pressed down then do a raycast into the scene
        if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                // GUI Action
                return;
            }

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, MAX_RAYCAST_DISTANCE))
            {
                // if we hit an object that has a mesh collider then select it
                SelectPart(hit.transform.gameObject);
            }
            else if(!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                // we don't want to deselect the part if we are clicking on a UI element
                DeselectPart();
            }
        }
    }

    /// <summary>
    /// Loads a specific existing configuration given a ConfigurationViewModel
    /// </summary>
    /// <param name="configVM">The configuration that needs to be loaded</param>
    public void LoadConfiguration(ConfigurationViewModel configVM)
    {
        DeselectPart();

        Debug.Log("Loading configuration name: " + configVM.Name + " with id: " + configVM.Id);

        var partNames = partManager.LoadConfiguration(configVM.Id);

        // remove all parts from the current configuration
        DestroyAllParts();

        // create new instances of each part
        foreach (var partName in partNames)
        {
            Part partPrefab = partManager.FindPartByName(partName);
            if (partPrefab)
            {
                var prefabObject = Instantiate(partPrefab);
                prefabObject.transform.parent = Configurator.Instance.customVehicleRoot.transform;
                prefabObject.name = partName;
                partManager.AddCurrentPart(prefabObject);
            }
            else
            {
                Debug.Log("Error: couldn't find the part: " + partName);
            }
        }

        configurationPanel.SetInfoText("Successfully loaded config: " + configVM.Name);
    }

    /// <summary>
    /// Lists the saved configurations
    /// </summary>
    /// <returns>A list of the saved configurations</returns>
    public List<ConfigurationViewModel> ListSavedConfigurations()
    {
        return partManager.ListSavedConfigurations();
    }

    /// <summary>
    /// Randomize a vehicle configuration
    /// </summary>
    public void Randomize()
    {
        DestroyAllParts();

        Debug.Log("Randomizing!");

        // get all possible values of the part category enum
        var allPartCategories = Enum.GetValues(typeof(PartCategory)).Cast<PartCategory>().ToList();

        // iterate over each category and try to create a part from each
        foreach(var category in allPartCategories)
        {
            var categoryParts = partManager.GetAllPartsByCategory(category);

            Part partPrefab = null;
            Part possibleIncompatiblePart = null;

            // make sure that we are adhering to the PartCompatabilityValidator and that we aren't breaking any rules
            // while generating these random configs
            do
            {
                var randomIndex = UnityEngine.Random.Range(0, categoryParts.Count - 1);

                var partName = categoryParts[randomIndex].name;

                partPrefab = partManager.FindPartByName(partName);
            } while (!partCompatibilityValidator.HasCompatibility(partPrefab, out possibleIncompatiblePart));
            
            if (partPrefab)
            {
                var prefabObject = Instantiate(partPrefab);
                prefabObject.transform.parent = Configurator.Instance.customVehicleRoot.transform;
                prefabObject.name = partPrefab.name;
                partManager.AddCurrentPart(prefabObject);
            }
        }

        configurationPanel.SetInfoText("Creating random configuration...");
    }

    /// <summary>
    /// Creates a clone of the currently configured vehicle and allows the user to drive it around
    /// </summary>
    public void Drive()
    {
        if(isDriving)
        {
            // we don't want to start a new driving session without first cleaning up the old one (duplicate vehicles)
            DriveReset();
        }

        if(!bodyCollidersPrefab || !wheelsHubsPrefab)
        {
            var logText = "You must have Body Colliders Prefab and Wheels Hubs Prefab assigned to valid prefabs in order to drive the vehicle";
            Debug.LogError(logText);
            configurationPanel.SetInfoText(logText);
            return;
        }
        if(!environmentRoot)
        {
            var logText = "You must have a root environment assigned to valid prefabs in order to drive the vehicle";
            Debug.LogError(logText);
            configurationPanel.SetInfoText(logText);
        }

        Debug.Log("Creating driveable configured vehicle");

        isDriving = true;

        // don't show anything as selected so we can make a fresh clone without various selected materials shown
        DeselectPart();

        // display the environment
        environmentRoot.SetActive(true);

        // clone our existing vehicle
        driveableCenterMouseLookAtTarget = Instantiate(customVehicleRoot.transform.parent);
        var driveableCustomVehicleRoot = driveableCenterMouseLookAtTarget.Find("CustomVehicleRoot");

        // hide the configurable vehicle
        customVehicleRoot.transform.parent.gameObject.SetActive(false);

        // add a rigid body to the new vehicle
        var vehicleRigidbody = driveableCustomVehicleRoot.parent.gameObject.AddComponent<Rigidbody>();
        vehicleRigidbody.mass = 1000;
        vehicleRigidbody.drag = 0.1f;
        vehicleRigidbody.angularDrag = 0.05f;

        // set up the drivable camera
        mainCamera.GetComponent<AudioListener>().enabled = false;
        mainCamera.enabled = false;
        drivingCameraRoot = Instantiate(carCamera);
        drivingCameraRoot.transform.parent = driveableCenterMouseLookAtTarget;
        drivingCameraRoot.GetComponent<CarCam>().Init();

        if (!driveableCustomVehicleRoot)
        {
            Debug.LogError("Error could not find CustomVehicleRoot for driveable clone");
            return;
        }

        foreach(var childMeshCollider in driveableCustomVehicleRoot.GetComponentsInChildren<MeshCollider>())
        {
            // disable all mesh colliders on this vehicle, we will be adding some wheel and simple box ones later for the body
            childMeshCollider.enabled = false;
        }

        // get the wheels root
        var wheelsRoot = driveableCustomVehicleRoot.transform.Cast<Transform>().SingleOrDefault(transform => transform.GetComponent<Part>().category == PartCategory.Wheels);
        var tempWheelsRoot = new GameObject("TempWheelsRoot");

        // iterate over each wheel prefab and add it to a container object, deals with problems with wheel rotation so putting the wheel inside of an
        // empty game object in the right orientation allows us to use the premade unity scripts
        var wheelsChildren = new List<Transform>(wheelsRoot.GetComponentsInChildren<Transform>().Where(child => child != wheelsRoot));
        var wheelContainers = new List<GameObject>();
        foreach (Transform wheel in wheelsChildren)
        {
            var wheelParentContainer = new GameObject(wheel.name + "_Container");
            wheelParentContainer.transform.parent = tempWheelsRoot.transform;
            wheelParentContainer.transform.localPosition = wheel.localPosition;
            wheelContainers.Add(wheelParentContainer.gameObject);

            // move the wheel over to exist under our new wheel container
            wheel.parent = wheelParentContainer.transform;
            wheel.localPosition = Vector3.zero;
        }

        // make our temp wheels root the new official root
        tempWheelsRoot.name = wheelsRoot.name;
        tempWheelsRoot.transform.parent = wheelsRoot.parent;

        // remove the original root since we don't need it anymore, it has been replaced by tempwheelsroot
        Destroy(wheelsRoot.gameObject);

        // create instances of body colliders and wheels hubs
        var bodyCollidersObject = Instantiate(bodyCollidersPrefab);
        var wheelsHubObject = Instantiate(wheelsHubsPrefab);

        // topmost root for the vehicle
        var driveableRoot = driveableCustomVehicleRoot.parent;

        // set them to be a child of the driveable looktarget and set their localposition's to (0, 0, 0)
        bodyCollidersObject.transform.parent = driveableRoot;
        wheelsHubObject.transform.parent = driveableRoot;
        bodyCollidersObject.transform.localPosition = Vector3.zero;
        wheelsHubObject.transform.localPosition = Vector3.zero;

        // create car controller and initialize its values
        var carController = driveableRoot.gameObject.AddComponent<CarController>();
        carController.wheelColliders = wheelsHubObject.GetComponentsInChildren<WheelCollider>().Where(child => child != wheelsHubObject).ToArray();
        carController.wheelMeshes = tempWheelsRoot.transform.Cast<Transform>().SelectMany(t => t.GetComponents<Transform>().Select(transform => transform.gameObject)).ToArray();
        carController.wheelEffects = wheelsHubObject.GetComponentsInChildren<WheelEffects>().Where(child => child != wheelsHubObject).ToArray();
        carController.maximumSteerAngle = 25.0f;
        carController.steerHelper = 0.644f;
        carController.tractionControl = 1;
        carController.fullTorqueOverAllWheels = 5000;
        carController.reverseTorque = 500;
        carController.maxHandbrakeTorque = 1e+08f;
        carController.topspeed = 300;
        carController.slipLimit = 0.3f;
        carController.brakeTorque = 20000;

        driveableRoot.gameObject.AddComponent<CarUserControl>();
    }

    /// <summary>
    /// Takes the user back into configuring mode
    /// </summary>
    public void DriveReset()
    {
        if (driveableCenterMouseLookAtTarget)
        {
            Destroy(driveableCenterMouseLookAtTarget.gameObject);
        }

        if(drivingCameraRoot)
        {
            Destroy(drivingCameraRoot.gameObject);
        }

        if(environmentRoot)
        {
            environmentRoot.SetActive(false);
        }

        mainCamera.enabled = true;
        mainCamera.GetComponent<AudioListener>().enabled = true;
        customVehicleRoot.transform.parent.gameObject.SetActive(true);

        isDriving = false;
    }

    /// <summary>
    /// Destroys every part that exists for the current configuration
    /// </summary>
    private void DestroyAllParts()
    {
        var currentParts = partManager.currentConfigurationParts;

        foreach(var currentPart in currentParts)
        {
            Destroy(currentPart.gameObject);
        }

        partManager.ClearAllParts();
    }

    /// <summary>
    /// Selects a part
    /// </summary>
    /// <param name="partObject">The game object of the part that has just been selected</param>
    private void SelectPart(GameObject partObject)
    {
        if (isDriving)
        {
            // don't allow the selecting of a part while driving
            return;
        }

        // if we have something already selected then deselect it first
        if (currentPart)
        {
            NotifyCurrentPartDeselected();
        }

        // set the current part to either the current part component or the parent part component
        // TODO: support more than two levels of game objects that have part components
        currentPart = partObject.transform.parent.GetComponent<Part>() ? partObject.transform.parent.GetComponent<Part>() : partObject.GetComponent<Part>();

        //Debug.Log("You've selected the " + currentPart.name, currentPart.gameObject); // ensure that you picked right object

        NotifyCurrentPartSelected();

        // update the configuration panel UI
        var categoryParts = partManager.GetPartsByCategory(currentPart.category, currentPart);
        configurationPanel.SetPartSelected(currentPart.name, currentPart.category.ToDescription(), categoryParts);
    }

    /// <summary>
    /// Deselects a part
    /// </summary>
    private void DeselectPart()
    {
        // make sure the existing part is deselected
        if (currentPart)
        {
            NotifyCurrentPartDeselected();
        }

        currentPart = null;

        // set UI text
        configurationPanel.SetPartDeselected();
    }

    /// <summary>
    /// Swaps out existing currentPart with the one specified by newPart
    /// </summary>
    /// <param name="newPart"></param>
    private void ReplaceCurrentPart(Part newPart)
    {
        Part possibleIncompatiblePart;
        if (partCompatibilityValidator.HasCompatibility(newPart, out possibleIncompatiblePart))
        {
            configurationPanel.SetInfoText("Replacing " + currentPart.name + " with " + newPart.name);

            partManager.RemoveCurrentPart(currentPart);

            // remove the existing part from the scene
            Destroy(currentPart.gameObject);

            // create the new part in the scene
            GameObject replacementPart = Instantiate(newPart.gameObject);
            replacementPart.transform.SetParent(customVehicleRoot.transform);

            // update the currentpart so that everyone who uses this will have the most up-to-date version of it
            currentPart = replacementPart.GetComponent<Part>();

            // remove (clone) from the name
            replacementPart.name = newPart.name;

            SelectPart(replacementPart);

            partManager.AddCurrentPart(currentPart);
        }
        else
        {
            configurationPanel.SetInfoText("Error: The parts " + newPart.name + " and " + possibleIncompatiblePart.name + " are incompatible.");
            NotifyCurrentPartIncompatible();
        }
    }

    /// <summary>
    /// Makes sure that the configurator has been set up for it to work correctly
    /// </summary>
    private void ValidateInit()
    {
        Debug.Assert(selectedMaterial, "The configurator must have a Selected Material assigned to it.", gameObject);
        Debug.Assert(incompatibleMaterial, "The configurator must have an Incompatible Material assigned to it.", gameObject);
        Debug.Assert(configurationPanel, "The configuration panel must have a valid instance set to it.", gameObject);
    }

    #region set current parts to various materials
    /// <summary>
    /// Tell the current part (and its children) that it has been selected
    /// </summary>
    private void NotifyCurrentPartSelected()
    {
        currentPart.MarkSelected();

        // some root prefabs don't have a renderer attached to them (Mil_2_Body_2) so make sure to call Select on the children too
        foreach (var partInChildren in currentPart.GetComponentsInChildren<Part>())
        {
            partInChildren.MarkSelected();
        }
    }

    /// <summary>
    /// Tell the current part (and its children) that it has been deselected
    /// </summary>
    private void NotifyCurrentPartDeselected()
    {
        currentPart.ResetMaterial();

        foreach (var partInChildren in currentPart.GetComponentsInChildren<Part>())
        {
            partInChildren.ResetMaterial();
        }
    }

    /// <summary>
    /// Tell the current part (and its children) that it is incompatible
    /// </summary>
    private void NotifyCurrentPartIncompatible()
    {
        currentPart.MarkIncompatible();

        // some root prefabs don't have a renderer attached to them (Mil_2_Body_2) so make sure to call Select on the children too
        foreach (var partInChildren in currentPart.GetComponentsInChildren<Part>())
        {
            partInChildren.MarkIncompatible();
        }
    }
    #endregion
}
