﻿using System;
using System.ComponentModel;
using UnityEngine;

/// <summary>
/// Provides methods that extend the functionality of their respective existing classes
/// </summary>
public static class HelperExtensions
{
    /// <summary>
    /// Thanks for this nice solution: http://www.luispedrofonseca.com/unity-quick-tips-enum-description-extension-method/
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToDescription(this Enum value)
    {
        DescriptionAttribute[] da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
        return da.Length > 0 ? da[0].Description : value.ToString();
    }

    /// <summary>
    /// Since we can't serialize lists by default using the JsonUtility, I found this nice helper from: https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity/36244111#36244111
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Converts to an object from json
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="json">The json data</param>
        /// <returns>An array of T type objects</returns>
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        /// <summary>
        /// Converts to json from an array of T objects
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="array">The objects</param>
        /// <returns>A json string</returns>
        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        /// <summary>
        /// Converts to json from an array of T objects, but print things pretty.
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="array">The objects</param>
        /// <param name="prettyPrint">True to print pretty!</param>
        /// <returns></returns>
        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        /// <summary>
        /// A wrapper object for the helpers to use
        /// </summary>
        /// <typeparam name="T"></typeparam>
        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
}
