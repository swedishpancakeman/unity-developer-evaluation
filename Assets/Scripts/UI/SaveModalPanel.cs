﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents the logic behind the save modal
/// </summary>
public class SaveModalPanel : MonoBehaviour
{
    #region inspector variables
    public Text configurationName;
    public Button saveButton;
    public Button cancelButton;
    public GameObject modalPanelObject;
    #endregion

    private Action<string> onSave;

    /// <summary>
    /// Single instance of this class that other classes can use
    /// </summary>
    private static SaveModalPanel saveModalPanel;
    public static SaveModalPanel Instance()
    {
        if (!saveModalPanel)
        {
            saveModalPanel = FindObjectOfType(typeof(SaveModalPanel)) as SaveModalPanel;
            if (!saveModalPanel)
                Debug.LogError("There needs to be one active SaveModalPanel script on a GameObject in your scene.");
        }

        return saveModalPanel;
    }

    /// <summary>
    /// Wires up actions either their respective buttons, stores onSave action to be called later
    /// </summary>
    /// <param name="onSave"></param>
    public void Init(Action<string> onSave)
    {
        this.onSave = onSave;

        saveButton.onClick.RemoveAllListeners();
        saveButton.onClick.AddListener(Save);
        saveButton.onClick.AddListener(ClosePanel);

        cancelButton.onClick.RemoveAllListeners();
        cancelButton.onClick.AddListener(ClosePanel);
    }

    /// <summary>
    /// Show the modal
    /// </summary>
    public void ShowModal()
    {
        modalPanelObject.SetActive(true);
    }

    /// <summary>
    /// Called when the save button is clicked
    /// </summary>
    void Save()
    {
        // call the action specified in onsave
        this.onSave(configurationName.text);
    }

    /// <summary>
    /// Closes the panel
    /// </summary>
    void ClosePanel()
    {
        modalPanelObject.SetActive(false);
    }
}
