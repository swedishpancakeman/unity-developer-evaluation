﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents the logic behind the load modal
/// </summary>
public class LoadModalPanel : MonoBehaviour
{
    #region inspector variables
    public Button cancelButton;
    public GameObject modalPanelObject;
    public GameObject scrollViewContentRoot;
    public GameObject configurationButtonPrefab;
    #endregion

    private Action<ConfigurationViewModel> onLoad;

    private List<GameObject> currentButtons = new List<GameObject>();

    /// <summary>
    /// Single instance of this class that other classes can use
    /// </summary>
    private static LoadModalPanel loadModalPanel;
    public static LoadModalPanel Instance()
    {
        if (!loadModalPanel)
        {
            loadModalPanel = FindObjectOfType(typeof(LoadModalPanel)) as LoadModalPanel;
            if (!loadModalPanel)
                Debug.LogError("There needs to be one active LoadModalPanel script on a GameObject in your scene.");
        }

        return loadModalPanel;
    }

    /// <summary>
    /// Stores the onload action for later use, hooks up cancel button
    /// </summary>
    /// <param name="onLoad"></param>
    public void Init(Action<ConfigurationViewModel> onLoad)
    {
        this.onLoad = onLoad;

        cancelButton.onClick.RemoveAllListeners();
        cancelButton.onClick.AddListener(ClosePanel);
    }

    /// <summary>
    /// Displays the modal and populates the list of available configurations to load
    /// </summary>
    /// <param name="configs"></param>
    public void ShowModal(List<ConfigurationViewModel> configs)
    {
        modalPanelObject.SetActive(true);

        RemoveAllButtons();

        PopulateConfigurationButtons(configs);
    }

    /// <summary>
    /// Creates the buttons that allow the user to load configurations
    /// </summary>
    /// <param name="configs"></param>
    private void PopulateConfigurationButtons(List<ConfigurationViewModel> configs)
    {
        foreach(var config in configs)
        {
            var configButtonObject = Instantiate(configurationButtonPrefab);

            currentButtons.Add(configButtonObject);

            var button = configButtonObject.GetComponent<UnityEngine.UI.Button>();

            configButtonObject.transform.SetParent(scrollViewContentRoot.transform, false);
            button.GetComponentInChildren<Text>().text = config.Name;

            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => this.onLoad(config));
            button.onClick.AddListener(ClosePanel);
        }
    }

    /// <summary>
    /// Removes all listed configuration buttons
    /// </summary>
    private void RemoveAllButtons()
    {
        foreach (var button in currentButtons)
        {
            Destroy(button);
        }
    }

    /// <summary>
    /// Closes panel
    /// </summary>
    void ClosePanel()
    {
        modalPanelObject.SetActive(false);
    }
}
