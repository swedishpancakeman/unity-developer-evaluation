﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents the configuration panel, this is shown on the right side of the screen
/// </summary>
public class ConfigurationPanel : MonoBehaviour
{
    #region inspector variables
    public Text categoryValueText;
    public Text infoValueText;
    public Text partNameValueText;
    public GameObject scrollViewContentRoot;
    public GameObject partButtonPrefab;
    public GameObject saveLoadRandomizeButtonPanel;
    #endregion

    private List<GameObject> currentButtons = new List<GameObject>();

    private Action<Part> onPartSelected;
    private SaveModalPanel saveModalPanel;
    private LoadModalPanel loadModalPanel;

    private const float PART_BUTTONS_Y_AXIS_START_POSITION = -30.0f;

    void Awake ()
    {
        saveModalPanel = SaveModalPanel.Instance();
        loadModalPanel = LoadModalPanel.Instance();

        ValidateInit();
    }

    /// <summary>
    /// Makes sure that the configurator has been set up for it to work correctly
    /// </summary>
    private void ValidateInit()
    {
        Debug.Assert(categoryValueText, "The configuration panel must have a valid text instance set for Category Value Text", gameObject);
        Debug.Assert(infoValueText, "The configuration panel must have a valid text instance set for Info Value Text", gameObject);
        Debug.Assert(partNameValueText, "The configuration panel must have a valid text instance set for Part Name Value Text", gameObject);
    }

    /// <summary>
    /// Sets the action that is to be used when a new part is selected to replace another one
    /// </summary>
    /// <param name="onPartSelected">A method that takes a Part as a parameter</param>
    public void Init(Action<Part> onPartSelected, Action<string> onSave, Action<ConfigurationViewModel> onLoad)
    {
        this.onPartSelected = onPartSelected;
        saveModalPanel.Init(onSave);
        loadModalPanel.Init(onLoad);
    }

    public void DisableSaveLoadRandomizePanel()
    {
        saveLoadRandomizeButtonPanel.SetActive(false);
    }

    public void EnableSaveLoadRandomizePanel()
    {
        saveLoadRandomizeButtonPanel.SetActive(true);
    }

    /// <summary>
    /// Makes the save panel display
    /// </summary>
    public void ShowSavePanel()
    {
        saveModalPanel.ShowModal();
    }

    /// <summary>
    /// Makes the load panel display
    /// </summary>
    public void ShowLoadPanel()
    {
        var savedConfigs = Configurator.Instance.ListSavedConfigurations();
        loadModalPanel.ShowModal(savedConfigs);
    }

    /// <summary>
    /// Sets the part name, category, and refreshes the available part options
    /// </summary>
    /// <param name="partName"></param>
    /// <param name="category"></param>
    /// <param name="partOptions"></param>
    public void SetPartSelected(string partName, string category, List<Part> partOptions)
    {
        SetPartNameText(partName);
        SetCategoryText(category);

        DisplayConfigurablePartOptions(partOptions);
    }

    /// <summary>
    /// Sets the part and category text to "None Available" and removes all available configurable part buttons
    /// </summary>
    public void SetPartDeselected()
    {
        var noneText = "None Selected";

        SetPartNameText(noneText);
        SetCategoryText(noneText);

        // don't show any replacement options since we now have no parts selected
        RemoveAllButtons();
    }

    #region Set info text
    /// <summary>
    /// Used for displaying information to the user in the configuration panel
    /// </summary>
    /// <param name="text">The text to display to the user</param>
    public void SetInfoText(string text)
    {
        SetInfoText(text, Color.white);
    }

    /// <summary>
    /// Used for displaying error information to the user in the configuration panel
    /// </summary>
    /// <param name="text">The error text to display to the user</param>
    public void SetInfoTextError(string error)
    {
        SetInfoText(error, Color.red);
    }

    /// <summary>
    /// Used for displaying information to the user in the configuration panel with a certain color
    /// </summary>
    /// <param name="info">The text to display to the user</param>
    /// <param name="color">The color that the text should be</param>
    public void SetInfoText(string info, Color color)
    {
        if (infoValueText)
        {
            infoValueText.color = color;
            infoValueText.text = info;
        }
    }
    #endregion

    /// <summary>
    /// Removes the existing available part buttons from the configuration panel and creates new ones
    /// based on what is passed in through categoryPartOptions.
    /// </summary>
    /// <param name="categoryPartOptions">The possible part options to replace a specific part with</param>
    private void DisplayConfigurablePartOptions(List<Part> categoryPartOptions)
    {
        // remove all buttons from the UI since we will be creating new ones
        RemoveAllButtons();

        //TODO: instead of doing this math, use Content Size Fitter + Vertical Layout Group
        var currentYButtonPosition = PART_BUTTONS_Y_AXIS_START_POSITION;
        foreach (var part in categoryPartOptions)
        {
            currentYButtonPosition = AddButton(part, currentYButtonPosition);
        }
    }

    /// <summary>
    /// Creates a new part button
    /// </summary>
    /// <param name="part">The part to create the button for</param>
    /// <param name="yStart">The top of where the button should be placed</param>
    /// <returns>The y-axis value that the next button can use for its starting placement</returns>
    private float AddButton(Part part, float yStart)
    {
        GameObject partButtonGO = Instantiate(partButtonPrefab);

        currentButtons.Add(partButtonGO);

        var button = partButtonGO.GetComponent<UnityEngine.UI.Button>();

        // attach the button as a child of the scrollViewContentRoot game object
        partButtonGO.transform.SetParent(scrollViewContentRoot.transform, false);
        partButtonGO.transform.localPosition = new Vector3(0.0f, yStart, 0.0f);
        var buttonHeight = button.GetComponent<RectTransform>().rect.height;
        button.GetComponentInChildren<Text>().text = part.name;

        // when we click on this button, pass in the new part that we will be using
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => this.onPartSelected(part));

        return yStart - button.GetComponent<RectTransform>().rect.height;
    }

    /// <summary>
    /// Remove all buttons from the current UI
    /// </summary>
    private void RemoveAllButtons()
    {
        foreach (var button in currentButtons)
        {
            Destroy(button);
        }
    }

    /// <summary>
    /// Sets the category text on the configuration panel
    /// </summary>
    /// <param name="category">The name of the part category</param>
    private void SetCategoryText(string category)
    {
        if (categoryValueText)
        {
            categoryValueText.text = category;
        }
    }

    /// <summary>
    /// Sets the part name text on the configuration panel
    /// </summary>
    /// <param name="name">The name of the part</param>
    private void SetPartNameText(string name)
    {
        if (partNameValueText)
        {
            partNameValueText.text = name;
        }
    }
}
