﻿using UnityEngine;

/// <summary>
/// Represents a part component that is attached to all vehicle piece game objects
/// </summary>
public class Part : MonoBehaviour
{
    #region inspector variables
    public PartCategory category;
    #endregion

    private Material originalMaterial;
    private new Renderer renderer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();

        if (renderer)
        {
            originalMaterial = renderer.material;
        }
    }

    /// <summary>
    /// Changes the material to be the selected material that is assigned through the Configurator game object
    /// </summary>
    public void MarkSelected()
    {
        if (renderer && Configurator.Instance.selectedMaterial)
        {
            renderer.material = Configurator.Instance.selectedMaterial;
        }
    }

    /// <summary>
    /// Changes the material to be the original material that was initially set
    /// </summary>
    public void ResetMaterial()
    {
        if (renderer)
        {
            renderer.material = originalMaterial;
        }
    }

    /// <summary>
    /// Changes the material to be the incompatibility material that is assigned through the Configurator game object
    /// </summary>
    public void MarkIncompatible()
    {
        if (renderer && Configurator.Instance.incompatibleMaterial)
        {
            renderer.material = Configurator.Instance.incompatibleMaterial;
        }
    }
}
