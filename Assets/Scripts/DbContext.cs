﻿using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;
using System;
using System.IO;

/// <summary>
/// Represents the data layer that saves, lists, and loads configurations to a database using SQLite
/// </summary>
public class DbContext
{
    public const string databaseFileName = "configdb.s3db";
    public readonly string assetPath = Application.dataPath + "/StreamingAssets/" + databaseFileName;
    public readonly string dataDirectory = Application.persistentDataPath + "/Database/";
    public readonly string dataPath = string.Empty;
    public readonly string databaseFile = string.Empty;

    public string LOAD_CONFIGURATION_QUERY = "SELECT data FROM configurations WHERE id={0}";
    public string SAVE_CONFIGURATION_QUERY = "INSERT INTO configurations (name, data) VALUES (@name, @data)";
    public const string SAVED_CONFIGURATIONS_QUERY = "SELECT id, name FROM configurations";

    /// <summary>
    /// Constructor
    /// </summary>
    public DbContext()
    {
        // set up paths
        dataPath = dataDirectory + databaseFileName;
        databaseFile = "URI=file:" + dataPath;

        CopyDatabaseFile();

        Debug.Log("Attempting to load database from file: " + databaseFile);
    }

    /// <summary>
    /// Saves a configuration to the database
    /// </summary>
    /// <param name="name">The configuration name</param>
    /// <param name="json">The configuration data</param>
    /// <returns></returns>
    public bool SaveConfiguration(string name, string json)
    {
        using (var dbConnection = (IDbConnection)new SqliteConnection(databaseFile))
        {
            dbConnection.Open();

            using (var cmd = dbConnection.CreateCommand())
            {
                cmd.CommandText = SAVE_CONFIGURATION_QUERY;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqliteParameter("@name", name));
                cmd.Parameters.Add(new SqliteParameter("@data", json));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Debug.LogError("Could not insert into database" + e);
                }
            }

            dbConnection.Close();
        }

        return true;
    }

    /// <summary>
    /// Loads a specific configuration given an id
    /// </summary>
    /// <param name="id">The id associated with the configuration</param>
    /// <returns>The JSON data for the configuration</returns>
    public string LoadConfiguration(int id)
    {
        var result = string.Empty;

        using (var dbConnection = (IDbConnection)new SqliteConnection(databaseFile))
        {
            dbConnection.Open();

            using (var cmd = dbConnection.CreateCommand())
            {
                cmd.CommandText = string.Format(LOAD_CONFIGURATION_QUERY, id);
                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    result = reader["data"].ToString();
                }
            }

            dbConnection.Close();
        }

        return result;
    }

    /// <summary>
    /// List all configurations currently saved to the database
    /// </summary>
    /// <returns>A list of configurations (ID and name)</returns>
    public List<ConfigurationViewModel> GetSavedConfigurations()
    {
        List<ConfigurationViewModel> data = new List<ConfigurationViewModel>();

        using (var dbConnection = (IDbConnection)new SqliteConnection(databaseFile))
        {
            dbConnection.Open();

            using (var cmd = dbConnection.CreateCommand())
            {
                cmd.CommandText = SAVED_CONFIGURATIONS_QUERY;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);  // name

                        var row = new ConfigurationViewModel()
                        {
                            Id = id,
                            Name = name
                        };

                        data.Add(row);
                    }
                }
            }

            dbConnection.Close();
        }

        return data;
    }

    /// <summary>
    /// Copies the database file into a persistent location so that saves can be made to it
    /// </summary>
    public void CopyDatabaseFile()
    {
        if (!File.Exists(dataPath))
        {
            // File doesn't exist, move it from assets folder to data directory
            Debug.Log("DB file doesn't exist in persistent directory, copying from " + assetPath + " to " + assetPath);
            System.IO.Directory.CreateDirectory(dataDirectory);
            File.Copy(assetPath, dataPath);
        }
    }
}

/// <summary>
/// Simple representation of a configuration, used by the load modal to specify which ID needs to be loaded
/// </summary>
public class ConfigurationViewModel
{
    /// <summary>
    /// The ID of the configuration
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// The name of the configuration
    /// </summary>
    public string Name { get; set; }
}
