﻿using UnityEngine;

/// <summary>
/// Slightly modified from http://wiki.unity3d.com/index.php?title=MouseOrbitImproved to include only orbiting when right clicking
/// </summary>
public class MouseOrbit : MonoBehaviour
{
    #region inspector variables
    public Transform target;

    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public float zoomIncrement = 5.0f;
    #endregion

    private const int RIGHT_MOUSE_BUTTON = 1;

    private Rigidbody rb;

    private float x = 0.0f;
    private float y = 0.0f;

    /// <summary>
    /// Initializes this component
    /// </summary>
    void Start()
    {
        // make sure we have a valid target to point at
        if(!target)
        {
            Debug.LogError("<color=red>Please assign a target to the mouse orbit script</color>", this);
            Debug.Break();
        }

        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        rb = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (rb != null)
        {
            rb.freezeRotation = true;
        }
    }
    
    /// <summary>
    /// Called after all Update methods on other MonoBehaviours have been called
    /// </summary>
    void LateUpdate()
    {
        if (target)
        {
            var scrollWheelValue = Input.GetAxis("Mouse ScrollWheel");

            // make sure that we are either pressing the right mouse button or scrolling with the scroll wheel
            if (Input.GetMouseButton(RIGHT_MOUSE_BUTTON) || scrollWheelValue != 0.0f)
            {
                // Get the mouse x/y values, multiply by some speed in either direction, and scale them down to a more reasonable number
                x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
            }
            // TODO: add some nice easing here?

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            // adjust distance using scrollwheel, in increments of "zoomIncrement" units
            distance = Mathf.Clamp(distance - scrollWheelValue * zoomIncrement, distanceMin, distanceMax);

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;

            transform.rotation = rotation;
            transform.position = position;
        }
    }

    /// <summary>
    /// Make sure that angle is always within min - max angles
    /// </summary>
    /// <param name="angle">Current angle</param>
    /// <param name="min">Minimum possible angle</param>
    /// <param name="max">Maximum possible angle</param>
    /// <returns></returns>
    public float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
        {
            angle += 360F;
        }

        if (angle > 360F)
        {
            angle -= 360F;
        }

        return Mathf.Clamp(angle, min, max);
    }
}